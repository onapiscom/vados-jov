import { load } from 'vados-i18n'
import * as en from './i18n/en.json'

load({ en })

export * from './Schema'
export * from './Validator'
