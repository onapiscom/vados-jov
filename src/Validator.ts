import { getMessage } from 'vados-i18n'

declare const global: any

export const validateFuns = new Map<string, Function>()

export function validate(data: any, schema: { [key: string]: any }, removeUnknown = true, lang = global['$i18nLanguage']) {
  const rs = removeUnknown ? {} : Object.assign({}, data) as any
  let map: Map<string, Promise<any>>

  for (const k in schema) {
    const v = schema[k]
    if (v._isIgnored) {
      if (!removeUnknown) delete rs[k]
      continue
    }

    let dk = data[k]
    let i = 0

    if (v._value !== undefined) {
      rs[k] = v._value
      continue
    }

    if (v._ref) {
      rs[k] = rs[v._ref]
      continue
    }

    if (dk === undefined) {
      if (v._isRequired) throw new Error(getMessage(lang, 'vados-jov__required', k))
      if (v._defaults) rs[k] = v._defaults
      continue
    } else if (v._isNotExisted) {
      throw new Error(getMessage(lang, 'vados-jov__notExisted', k))
    }
    for (const len = v._functions.length; i < len; i++) {
      const fc = v._functions[i]
      let fcd: any
      if (typeof fc !== 'function') {
        const tmp = validateFuns.get(fc.name)
        if (!tmp) throw new Error(getMessage(lang, 'vados-jov__getValidator', fc.name))
        fcd = tmp(dk, k, lang, ...fc.prms)
      } else {
        fcd = fc(dk, k, lang, rs)
      }
      if (fcd !== undefined) {
        if (fcd instanceof Promise) {
          if (!map) map = new Map<string, Promise<any>>()
          map.set(k, fcd)
        } else {
          dk = fcd
        }
      }
    }
    rs[k] = dk
  }
  if (map) {
    return Promise.all(map.values()).then(rsv => {
      const it = map.keys()
      for (let r of rsv) {
        if (r !== undefined) {
          rs[it.next().value] = r
        } else {
          it.next()
        }
      }
      return rs
    })
  }
  return rs
}
