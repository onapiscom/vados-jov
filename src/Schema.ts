import { validateFuns, validate } from './Validator'
import { getMessage } from 'vados-i18n'

const booleans = ['true', true, 1, '1', 'false', false, 0, '0']

export function JOV(fc): Function {
  return function (_target: any, name: string, _func: TypedPropertyDescriptor<Function>) {
    validateFuns.set(name, fc)
    return function (...prms) {
      return this.addValidator(name, ...prms)
    }
  }
}

export function extend<T>(f: T): Schema & T
export function extend<T, U>(f: T, s: U): Schema & T & U
export function extend<T, U, V>(f: T, s: U, t: V): T & U & V
export function extend<T, U, V, K>(f: T, s?: U, t?: V, r?: K): T & U & V & K {
  const merge = (s) => {
    Object.getOwnPropertyNames(s['prototype']).filter(e => !e.startsWith('_') && !['addValidator', 'getValidator'].includes(e)).forEach(name => {
      Schema.prototype[name] = s['prototype'][name];
    })
  }
  if (f) merge(f)
  if (s) merge(s)
  if (t) merge(t)
  if (r) merge(r)
  return Schema as any
}

export function jov() {
  return new Schema()
}

export class BaseSchema {
  _functions = [] as ({ name: string, prms?: any } | Function)[]
  _isNotExisted = false
  _isIgnored = false
  _isRequired = false
  _defaults: any
  _value: any
  _ref: string

  addValidator(name: string, ...prms: any[]) {
    this._functions.push({ name, prms })
    return this
  }
  getValidator(name: string, lang) {
    const fc = validateFuns.get(name)
    if (!fc) throw new Error(getMessage(lang, 'vados-jov__getValidator', name))
    return fc
  }
}

export class Schema extends BaseSchema {
  default(vl?: any) {
    this._defaults = vl
    return this
  }
  notExisted() {
    this._isNotExisted = true
    return this
  }
  ignored() {
    this._isIgnored = true
    return this
  }
  required() {
    this._isRequired = true
    return this
  }
  ref(ref) {
    this._ref = ref
    return this
  }
  set(value) {
    this._value = value
    return this
  }
  manual(fc: (data, key, obj) => any) {
    this._functions.push(fc)
    return this
  }

  @JOV((vl, key, lang) => { // opts: ['date`, 'jsonstring']
    if (vl === null) return vl
    if (typeof vl !== 'string') throw new Error(getMessage(lang, 'vados-jov__string', key))
    return vl
  })
  string() {
    return this.addValidator('string')
  }

  @JOV((vl, key, lang) => {
    if (vl === null) return vl
    if (typeof vl !== 'number') throw new Error(getMessage(lang, 'vados-jov__number', key))
    return vl
  })
  number() {
    return this.addValidator('number')
  }

  @JOV((vl, key, lang) => {
    if (vl === null) return vl
    if (typeof vl !== 'boolean') throw new Error(getMessage(lang, 'vados-jov__boolean', key))
    return vl
  })
  boolean() {
    return this.addValidator('boolean')
  }

  @JOV((vl, key, lang) => {
    if (vl === null) return vl
    if (!(vl instanceof Date)) throw new Error(getMessage(lang, 'vados-jov__date', key))
    return vl
  })
  date() {
    return this.addValidator('date')
  }

  @JOV((vl, key, lang) => {
    if (vl === null) return vl
    if (!Array.isArray(vl)) throw new Error(getMessage(lang, 'vados-jov__array', key))
    return vl
  })
  array() {
    return this.addValidator('array')
  }

  @JOV((vl, key, lang) => {
    if (vl === null) return vl
    if (vl.constructor !== Object) throw new Error(getMessage(lang, 'vados-jov__object', key))
    return vl
  })
  object() {
    return this.addValidator('object')
  }

  @JOV((vl, key, lang, schema, isRemoveUnknown) => {
    if (vl === null) return vl
    try {
      if (Array.isArray(vl)) {
        let map: Map<number, Promise<any>>
        for (let i = 0, len = vl.length; i < len; i++) {
          vl[i] = validate(vl[i], schema, isRemoveUnknown, lang)
          if (vl[i] instanceof Promise) {
            if (!map) map = new Map<number, Promise<any>>()
            map.set(i, vl[i])
          }
        }
        if (map) {
          return Promise.all(map.values()).then(rsv => {
            const it = map.keys()
            for (let r of rsv) {
              if (r !== undefined) {
                vl[it.next().value] = r
              } else {
                it.next()
              }
            }
            return vl
          })
        }
        return vl
      } else {
        return validate(vl, schema, isRemoveUnknown, lang)
      }
    } catch (e) {
      throw new Error(`${key}.${e.message}`)
    }
  })
  validate(schema: object, isRemoveUnknown?: boolean) {
    return this.addValidator('validate', schema, isRemoveUnknown)
  }

  @JOV((vl, key, lang, ...types: any[]) => {
    if (vl === null) return vl
    const msgs = []
    for (const type of types) {
      try {
        const rs = validate({ [key]: vl }, { [key]: type }, undefined, lang)
        if (rs instanceof Promise) return rs.then(rs => rs[key])
        return rs[key]
      } catch (e) {
        msgs.push(e.message)
      }
    }
    throw new Error(`${msgs.join(getMessage(lang, 'vados-jov__or'))}`)
  })
  or(...types: any[]) {
    return this.addValidator('or', ...types)
  }

  @JOV((vl, key, lang, ...prms: any[]) => {
    if (vl === null) return vl
    if (prms.indexOf(vl) === -1) throw new Error(getMessage(lang, 'vados-jov__in', key, prms))
    return vl
  })
  in(...prms: any[]) {
    return this.addValidator('in', ...prms)
  }

  @JOV((vl, key, lang, ...prms: any[]) => {
    if (vl === null) return vl
    if (prms.indexOf(vl) !== -1) throw new Error(getMessage(lang, 'vados-jov__notIn', key, prms))
    return vl
  })
  notIn(...prms: any[]) {
    return this.addValidator('notIn', ...prms)
  }

  @JOV(vl => {
    if (vl === null) return vl
    return vl.toLowerCase()
  })
  toLowerCase() {
    return this.addValidator('toLowerCase')
  }

  @JOV(vl => {
    if (vl === null) return vl
    return vl.toUpperCase()
  })
  toUpperCase() {
    return this.addValidator('toUpperCase')
  }

  @JOV((vl, key, lang) => {
    if (vl === null) return vl
    const checker = validateFuns.get('string')
    let rs = vl
    try {
      return checker(rs, key, lang)
    } catch (e) {
      return checker(rs.toString(), key, lang)
    }
  })
  toString() {
    return this.addValidator('toString')
  }

  @JOV((vl, key, lang) => {
    if (vl === null) return vl
    const checker = validateFuns.get('number')
    try {
      return checker(vl, key, lang)
    } catch (e) {
      return checker(+vl, key, lang)
    }
  })
  toNumber() {
    return this.addValidator('toNumber')
  }

  @JOV((vl, key, lang) => {
    if (vl === null) return vl
    const checker = validateFuns.get('boolean')
    try {
      return checker(vl, key, lang)
    } catch (e) {
      const idx = booleans.indexOf(vl)
      if (idx !== -1) {
        vl = idx <= 3
      }
      return checker(vl, key, lang)
    }
  })
  toBoolean() {
    return this.addValidator('toBoolean')
  }

  @JOV((vl, key, lang) => {
    if (vl === null) return vl
    const checker = validateFuns.get('date')
    try {
      return checker(vl, key, lang)
    } catch (e) {
      return checker(new Date(vl), key, lang)
    }
  })
  toDate() {
    return this.addValidator('toDate')
  }

  @JOV((vl, key, lang) => {
    if (vl === null) return vl
    const checker = validateFuns.get('array')
    try {
      return checker(vl, key, lang)
    } catch (e) {
      if (typeof vl !== 'string') throw e
      try {
        vl = JSON.parse(vl)
      } catch (e) {
        throw new Error(getMessage(lang, 'vados-jov__toArray', key))
      }
      return checker(vl, key, lang)
    }
  })
  toArray() {
    return this.addValidator('toArray')
  }

  @JOV((vl, key, lang, jsonInString?: boolean, dateInString?: boolean) => {
    if (vl === null) return vl
    const checker = validateFuns.get('object')
    try {
      checker(vl, key, lang)
    } catch (e) {
      if (typeof vl !== 'string') throw e
      try {
        vl = JSON.parse(vl)
      } catch (e) {
        throw new Error(getMessage(lang, 'vados-jov__toObject', key))
      }
      checker(vl, key, lang)
    }
    let opts
    if (jsonInString || dateInString) {
      opts = { jsonInString, dateInString }
    }
    return toObject(vl, opts)
  })
  toObject(jsonInString?: boolean, dateInString?: boolean) {
    return this.addValidator('toObject', [jsonInString, dateInString])
  }

  @JOV((vl, key, lang) => {
    if (vl === null) return vl
    if (vl !== undefined) {
      if (typeof vl === 'string') {
        vl = vl.trim()
        if (vl.length > 0) return vl
      } else if (Array.isArray(vl) && vl.length > 0) {
        return vl
      } else if (vl.constructor === Object && Object.keys(vl).length > 0) {
        return vl
      }
    }
    throw new Error(getMessage(lang, 'vados-jov__notEmpty', key))
  })
  notEmpty() {
    return this.addValidator('notEmpty')
  }
}

// PRIVATE
function toObject(obj, opts: { jsonInString?: boolean, dateInString?: boolean }) {
  if (!obj) return obj
  for (const fieldName in obj) {
    let props = obj[fieldName]
    if (opts && typeof props === 'string') {
      if (opts.jsonInString && /^[\[\{]/.test(obj)) obj = JSON.parse(obj)
      if (opts.dateInString && /^\d{4}-\d{1,2}-\d{1,2}T\d{1,2}:\d{1,2}:\d{1,2}\.\d{1,3}Z$/.test(props)) {
        obj[fieldName] = new Date(props)
      }
    } else if (Array.isArray(props)) {
      obj[fieldName] = props.map(e => toObject(e, opts))
    } else if (props.constructor === Object) {
      obj[fieldName] = toObject(props, opts)
    }
  }
  return obj
}
