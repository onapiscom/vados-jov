# jov
Json Object Validator

# Installation
```nodejs
npm i vados-jov -S
```

# Examples

```sh
import { validate, Schema, jov, JOV } from './'

class MyScheme {

  @JOV()
  mytype(vl, key, mode, a) {
    console.log(vl, key, mode, a)
    if (vl === key) throw new Error('not equal key')
    return vl
  }
}

async function main() {
  validate({
    name: 123
  }, {
      name: jov().mytype('thanh', 'a')
    })
}

main()
```